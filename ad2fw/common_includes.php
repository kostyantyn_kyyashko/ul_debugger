<?
/**
 * Пути к классам фреймворка
 */
$all_path = array(
    '/core',
    '/Smarty',
);
foreach ($all_path as $path){
    $path = dirname(__FILE__).$path;
    set_include_path(get_include_path().PATH_SEPARATOR.$path);
}

/**
 * Подключение шаблонов и Smarty
 * Все нужные пути прописываются и файлы подключаются при создании объекта Smarty_Obj
 * Все пути относительные. Прописать требуется только пути к Smarty и шаблонам
 * Все пути - относительно корня веб-директории
 */

define('RELATIVE_PATH_TO_SMARTY', '/Smarty');

set_include_path(get_include_path().PATH_SEPARATOR.dirname(__FILE__).RELATIVE_PATH_TO_SMARTY);

function class_loader($className){

    //предотвращение конфликта с автолоадером Smarty
    if(strstr($className, 'Smarty')){  
        $className = strtolower($className);
    }

    include_once $className.'.php';
}

spl_autoload_register('class_loader');


