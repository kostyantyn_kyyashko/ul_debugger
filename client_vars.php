<?php

class Sender
{
    const URL = 'http://debug.nottes.net/errors';
    /**
     * get full data of web page, include header, coockies etc
     * @param $url
     * @param bool $is_post
     * @param string $post_fields
     * @param string $cookies_in
     *
     * @return mixed
     */
    private static function webPageGet($url, $is_post = true, $post_fields = '', $cookies_in = false, $noProxy = true){
        $options = [
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $post_fields
        ];
        $ch = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        return $body_content;
    }

    public static function send_error(array $post_data)
    {
        $url = static::URL;
        static::webPageGet($url, 1, $post_data);
    }
}
