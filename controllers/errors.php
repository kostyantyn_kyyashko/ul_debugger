<?php
class errors extends Controller
{
    public function run($req)
    {
        $req = array_merge($_GET, $_POST);
        $db = new Db();
        $db->insert('errors', [
            'project' => $req['project'],
            'level' => $req['level'],
            'message' => urlencode($req['message']),
            'file' => $req['file'],
            'line' => $req['line'],
            'timestamp' => time()
        ]);
    }
}
