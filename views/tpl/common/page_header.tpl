<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="icon" href="{$smarty.const.PROJECT_URL}/views/img/favicon.ico" type="image/x-icon" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <link type="text/css" rel="stylesheet" href="{$smarty.const.PROJECT_URL}/views/css/style.css"/>

    <script type="text/javascript" src="{$smarty.const.FULL_URL_TO_FW}/views/js/jquery.js"></script>
    <script type="text/javascript" src="{$smarty.const.FULL_URL_TO_FW}/views/js/jquery.cookie.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    {*Клиентские яваскрипты - массивом*}
    {if isset($js)}
        {foreach $js as $client_js}
        <script type="text/javascript" src="{$smarty.const.PROJECT_URL}/views/js/{$client_js}"></script>
        {foreachelse}
        {/foreach}
    {/if}

    {*Клиентские стили - кроме style.css, если есть конечно*}
    {if isset($css)}
        {foreach $css as $client_css}
            <link rel="stylesheet" type="text/css" href="{$smarty.const.PROJECT_URL}/views/css/{$client_css}"/>
        {foreachelse}
        {/foreach}
    {/if}


</head>
<body>
<div class="container">